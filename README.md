# Front-end Assignment 2: React

![image](https://gitlab.com/Frechr/assignment-two-hfc/-/raw/main/src/imgs/logo.png?raw=true)

#
## Assignment Requirements:
####
This assignment creates an online sign language translator using React as a Single Page Application. The application contains a Startup/Login page, Translation page, and Profile page, with the specific requirements for each page.

The required features include the React framework, React Router, and API to store users and their translations.
#
### Configuration :
#### The project uses the following tools:
- NPM/Node.js
- React (CRA)
- VS Code
- React dev tools
- Git
- CSS with Bootstrap and React-icons

###


### Authors:
####
- <a href="https://www.linkedin.com/in/h%C3%A5kon-solheim-379057175/">Håkon Natland Solheim</a>
- <a href="https://www.linkedin.com/in/fredrik-christensen-a33451159/">Fredrik Christensen</a>


