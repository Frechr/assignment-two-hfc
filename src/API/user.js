const apiURL = process.env.REACT_APP_API_URL;
const apiKey = process.env.REACT_APP_API_KEY;

const createHeaders = () => {
  return {
    "Content-Type": "application/json",
    "x-api-key": apiKey,
    "Accept-Encoding": "gzip, deflate, br",
  };
};

const GETUser = async (username) => {
  try {
    console.dir(username);
    console.dir(apiURL);
    const response = await fetch(`${apiURL}?username=${username.toString()}`);
    if (!response.ok) {
      return new Object(); //User not found, return empty object
    }
    try {
      const data = await response.json();
      return data[0];
    } catch (err) {
      throw ("Error when parsing user object: " + err);
    }
  } catch (err) {
    throw ("Error when sending request to API: " + err);
  }
};

const POSTUser = async (username) => {
  try {

    const response = await fetch(apiURL, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: [],
      }),
    });
    console.trace("POST USER: " + response);
    if (!response.ok) {
      throw new Error(response.status.toString());
    }
    const data = await response.json();
    return data;
  } catch (err) {
    alert(err);
    throw ("Wooopsie");
  }
};

export const getUser = GETUser;
export const createUser = POSTUser;