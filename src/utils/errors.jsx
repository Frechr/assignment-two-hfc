export const errors = {
  USER_NOT_FOUND: "\nYou are not registered with us.",
  USER_REGISTER_DECLINED: "\nFeel free to signup later!",
  USERNAME_LENGTH: `\nYour username is not long enough, it should be at least three characters long.`,
  USERNAME_SYMBOLS: "\nYour username contains invalid characters.",
  UNAUTHORIZED: "\nYou are not authorized to perform this action.",
  CLIENT_ERROR: "\nSomething went wrong while processing your request.",
  SERVER_ERROR: "\nSomething went wrong on the server, please try again later.",
  UNSPECIFIED: "\nSomething went wrong, please try again later.",
  USERNAME_INVALID: "\nSomething went wrong, please try again later.",
};
export const {
  USER_NOT_FOUND,
  USER_REGISTER_DECLINED,
  USERNAME_LENGTH,
  USERNAME_SYMBOLS,
  USERNAME_INVALID,
  UNAUTHORIZED,
  CLIENT_ERROR,
  SERVER_ERROR,
  UNSPECIFIED,
} = errors;
