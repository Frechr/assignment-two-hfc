import { Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";

export const NavBar = () => (
  <Navbar className="navbar navbar-expand-lg navbar-light bg-light ">
    <NavLink className="nav-link" to="/translate">
      Translate
    </NavLink>
    <NavLink className="nav-link" to="/profile">
      Profile
    </NavLink>
  </Navbar>
);
