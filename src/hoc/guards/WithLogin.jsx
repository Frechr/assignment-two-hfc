import { useAuthUser } from "../../contexts/AuthUser";
import { Navigate, NavLink, useNavigate } from "react-router-dom";

export const WithLoginRequired = (Component) => {
  const WrappedComponent = (props) => {
    const {isLoggedIn, logout} = useAuthUser();
    const navigate = useNavigate();
    if (isLoggedIn) { 
      return <>
      <nav className="mx-auto">
        <NavLink to={"/profile"}>Profile</NavLink>
        <NavLink to={"/translate"}>Translate</NavLink>
        <button onClick={()=>{
          logout(); 
        }}>Log out</button>
      </nav>
      <Component {...props} />
      </>
       ;
    } else {
      console.warn("Unauthorized: Requires login.");
      return <Navigate to="/" />;
    }
  };
  return WrappedComponent;
};

export default WithLoginRequired;
// NOTE: import like this: import { WithLoginRequired } from "../../hoc/guards/WithLogin";
// And use it when exporting a page like: export default WithLoginRequired(ProfilePage);
