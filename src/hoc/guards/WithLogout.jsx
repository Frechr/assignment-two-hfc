import { useAuthUser } from "../../contexts/AuthUser";
import { Navigate } from "react-router-dom";

export const WithLogoutRequired = (Component) => {
  const WrappedComponent = (props) => {
    const {isLoggedIn} = useAuthUser();
    if (!isLoggedIn) { 
      return <Component {...props} />;
    } else {
      console.warn("Unauthorized: Requires to be logged out.");
      return <Navigate to="/translate" />;
    } 
  };
  return WrappedComponent;
};

export default WithLogoutRequired;
// NOTE: import like this: import { WithLogoutRequired } from "../../hoc/guards/WithLogout";
// And use it when exporting a page like: export default WithLogoutRequired(LoginPage);
