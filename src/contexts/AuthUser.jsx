import React, { createContext, useContext, useState, useEffect } from "react"; 

const LOCAL_AUTH_KEY = "isLoggedIn";
const LOCAL_USER_KEY = "user";
const LOCAL_ERRORS_KEY = "input";

// AuthUserContext
export const AuthUserContext = createContext({
  errors: [],
  setErrors: () => {},
  user: {},
  setUser: () => {},
  isLoggedIn: false,
  setIsLoggedIn: () => {},
  logout: () => {},
});

export const AuthUserProvider = ({ children }) => {

  // Initialize values with useState
  const [isLoggedIn, setIsLoggedIn] = useState(() => {
    const authLocal = localStorage.getItem(LOCAL_AUTH_KEY);
    return authLocal ? JSON.parse(authLocal) : false;
  });

  const [user, setUser] = useState(() => {
    const userLocal = localStorage.getItem(LOCAL_USER_KEY);
    return userLocal ? JSON.parse(userLocal) : {};
  });

  const [errors, setErrors] = useState(() => {
    const errorsLocal = localStorage.getItem(LOCAL_ERRORS_KEY);
    return errorsLocal ? JSON.parse(errorsLocal) : [];
  });

  const logout = () => {
    setIsLoggedIn(false);
    localStorage.removeItem(LOCAL_AUTH_KEY);
    localStorage.removeItem(LOCAL_USER_KEY);
  }
  // I should probably use a reducer
  const contextValue = {
    errors,
    setErrors,
    user,
    setUser,
    isLoggedIn,
    setIsLoggedIn,
    logout
  }; 
    // Side effect: save to local storage, the login state and user
    useEffect(()=>{
      if(user && isLoggedIn){
        try {
          localStorage.setItem(LOCAL_AUTH_KEY, JSON.stringify(isLoggedIn));
        } catch (error) {
          console.error("LOCAL_AUTH_KEY couldn't be set!");
        }
        try {
          localStorage.setItem(LOCAL_USER_KEY, JSON.stringify(user));
        } catch (error) {
          console.error("LOCAL_USER_KEY couldn't be set!");
        }
      } 
    },[isLoggedIn, setIsLoggedIn, user, setUser]);
  return (
    <AuthUserContext.Provider value={contextValue??AuthUserContext}>
      {children}
    </AuthUserContext.Provider>
  );
};

export const useAuthUser = () => useContext(AuthUserContext);
