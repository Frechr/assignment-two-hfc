export const ErrorPage = () => (
  <div className="d-flex flex-row justify-content-centered align-content-center align-items-stretch  p-5 border">
    <div className="w-100">
      <h1>404 - Not Found</h1>
    </div>
    <div className="w-100">
      <h2>The page you are looking for is not here.</h2>
    </div>
  </div>
);
export default ErrorPage;
