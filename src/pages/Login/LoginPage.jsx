 import logo from "../../imgs/logo.png";
import { WithLogoutRequired } from "../../hoc/guards/WithLogout";
import { LoginForm } from "./Components/LoginForm";

const LoginPage = () => {
 
  // const handleErrors = (err) => {
  //   const pushErrorText = (err) => setErrorText(errorText + "\n" + err);
  //   switch (err) {
  //     case USER_NOT_FOUND:
  //       pushErrorText(USER_NOT_FOUND);
  //     break;
  //     case USER_REGISTER_DECLINED:
  //       pushErrorText(USER_REGISTER_DECLINED);
  //     //falls through
  //     case USERNAME_INVALID:
  //       pushErrorText(USERNAME_INVALID);
  //     //falls through
  //     case USERNAME_LENGTH:
  //       pushErrorText(USERNAME_LENGTH);
  //     //falls through
  //     case USERNAME_SYMBOLS:
  //       pushErrorText(USERNAME_SYMBOLS);
  //     //falls through
  //     default:
  //       pushErrorText("Something went wrong.");
  //       const id = setTimeout(() => {
  //         setErrorText(null);
  //         clearTimeout(id);
  //       }, 10000);
  //       break;
  //   }
  // };

  // const handleLogin = async () => {
  //   setLoading(true);
  //   if (!isValidName(username)) {
  //     try {
  //       throw new Error("Your username does not fit the requirements.");
  //     } catch (handleErrors) {
  //       await new Promise(async (resolve, reject) => {
  //         let createNewUser = await prompt(
  //           "Would you like to register a new user?"
  //         );
  //         createNewUser ? (
  //           Promise.resolve(await handleRegistration(username)) ? (
  //             <Navigate to={"/translate"} />
  //           ) : (
  //             handleErrors(SERVER_ERROR)
  //           )
  //         ) : (
  //           reject(USER_REGISTER_DECLINED)
  //         );
  //       });
  //     }
  //   } else {
  //     try {
  //       const user = await login(username);
  //       await navigateIfUserExist(user);
  //     } catch (error) {
  //       handleErrors(USER_NOT_FOUND);
  //     } finally {
  //       setLoading(false);
  //     }
  //   }
  // };

  // const handleRegistration = async () => {
  //   if (!isValidName(username)) {
  //     handleErrors(USERNAME_INVALID);
  //     return;
  //   } else {
  //     setLoading(true);
  //     try {
  //       const result = Promise.resolve(await register(username));
  //       if (result) {
  //         handleErrors(USER_REGISTER_DECLINED);
  //       } else {
  //         navigateIfUserExist(result.user);
  //       }
  //     } catch (error) {
  //       handleErrors(USERNAME_INVALID);
  //     } finally {
  //       setLoading(false);
  //     }
  //   }
  // };

  return (
    <div className="container bs-primary p-5 mx-auto">
      {
        // Website logo
      }
      <div className="row pb-5 w-100 mx-auto">
        <div className="col text-center">
          <img className="img-fluid" src={logo} alt="logo of translate page" />
        </div>
      </div>
      <hr />
      {
        // User login input field
      }
      <div id="user-login-bar" className="row mx-auto">
        <div className="col d-flex justify-content-center">
          {
            <LoginForm/>
          } 
        </div>
      </div>
    </div>
  );
};
export default WithLogoutRequired(LoginPage);
