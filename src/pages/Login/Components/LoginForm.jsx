import { useAuthUser } from "./../../../contexts/AuthUser";
import { useState } from "react";
import { getUser } from "../../../API/user";

export const LoginForm = () => {
  const { setIsLoggedIn , setUser} = useAuthUser(); 
  const usernameRegex =
    /^(?=.{3,20}$)(?![.-])(?!.*[.-]{2})[a-zA-Z0-9_-]+([^._-])$/;
  const [usernameValid, setUsernameValid] = useState(false);
  const [username, setUsername] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState([]); // Errors from API or validation
  const [hints, setHints] = useState([]); // Array of hints for username validtation

  // Update validity, hints and errors.
  const handleUserInput = (userInput) => {
    setUsername(userInput); //Required to update UI.
    if (usernameRegex.test(userInput) && userInput != null) {
      //Username valid
      setHints([]);
      setUsernameValid(true);
    } else {
      setHints(["Username is not valid."]);
    }
  };
  //Updates isLoggedIn and user context
  const handleLogin = async (event) => {
    event.preventDefault(); 
    setErrors([]);
    setIsLoading(true);
    try {  
      const user = await getUser(username); // Gets the user, emoty object if not found
      if (user != null ) { 
        setIsLoggedIn(true);
        setUser(user); 
      } else {
        setErrors(["Login failed: User not found."]);
      }
      // Error from getuser
    } catch (err) {
      setErrors([`Error when fetching from API: ${err}`]);
    }
    setIsLoading(false);
  };

  return (
    <form>
      <input
        onChange={(e) => {
          handleUserInput(e.target.value);
          e.preventDefault();
        }}
        disabled={isLoading}
        value={username}
      />
      <button
        onClick={async(e)=> await handleLogin(e)}
        disabled={isLoading || !usernameValid}
      />
      <br />
      {errors.map((err) => (
        <p className="text-danger" key={err}>
          {err}
          <br />
        </p>
      ))}
      <br />
      {hints.map((hint) => (
        <p className="text-warning" key={hint}>
          {hint}
          <br />
        </p>
      ))}
    </form>
  );
};

export default LoginForm;
