import React from "react";
import Profile from "../../components/Profile";
import { WithLoginRequired } from './../../hoc/guards/WithLogin';
import { useAuthUser } from './../../contexts/AuthUser';

const ProfilePage = ({children}) => {
  const {user} = useAuthUser();
  return <>
  <h2>{user.username}</h2>
  <Profile>{children}</Profile>
    {user.translations.slice().reverse().splice(0,10).map(trans=><p key={Math.random()}>{trans}</p>)}
  </>;
};

export default WithLoginRequired(ProfilePage);
