import React, { useState } from "react";
import { useAuthUser } from './../../../contexts/AuthUser';

const TranslateForm = () => {
  const [text, setText] = useState("");
  const {user, setUser} = useAuthUser();

  const handleInputChange = (event) => {
    setText(event.target.value);
    event.preventDefault();
  };

  // Push translations to user
  const handleTranslateClick = (e) => {
    e.preventDefault();
    let newUser = JSON.parse(JSON.stringify(user)); 
    newUser.translations.push(text); 
    setUser(newUser);
  };

  return (
    <div className="container container-md w-50">
      <input className="form " type="text" value={text} onChange={handleInputChange} />
      <button className="btn btn-primary" onClick={handleTranslateClick}>Translate</button>
      <br/>
      {text
        .split("")
        .map((c) => (
            c && 
          <img
            key={c + Math.random()*999999999999999} //Collisions may happen but meh
            src={`signs/${c}.png`}
            alt={`Sign icon representing the letter "${c}"`}
            className="img img-fluid"
          />
        )) }
    </div>
  );
};

export default TranslateForm;
