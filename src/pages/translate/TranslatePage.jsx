import React from "react";
import TranslateForm from "./components/TranslateForm";
import { WithLoginRequired } from "./../../hoc/guards/WithLogin";

const TranslationPage = () => {
  return (
    <>
      <h1>Translation</h1>
      <TranslateForm />
    </>
  );
};

export default WithLoginRequired(TranslationPage);
