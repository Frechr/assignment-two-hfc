import { BrowserRouter, Routes, Route } from "react-router-dom";
import ErrorPage from "./pages/error/ErrorPage"
import LoginPage from "./pages/Login/LoginPage"
import TranslatePage from "./pages/translate/TranslatePage";
import ProfilePage from "./pages/profile/ProfilePage"; 
import { AuthUserProvider } from './contexts/AuthUser';

function App() {
  return (
    <AuthUserProvider>
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route
          replace
          errorElement={<ErrorPage />}
          index
          element={<LoginPage />}
        />
          <Route
            errorElement={<ErrorPage />}
            replace
            path="/profile" 
            element={<ProfilePage />}
          /> 
          <Route
            errorElement={<ErrorPage />}
            replace
            path="/test" 
            element={<h1>TestPage</h1>}
          /> 
          <Route
            replace
            errorElement={<ErrorPage />}
            path="/translate"
            element={<TranslatePage/> }
          />
          <Route
            replace
            errorElement={<ErrorPage />}
            path="*"
            element={<ErrorPage />}
          /></Routes>
      </div>
    </BrowserRouter>
    </AuthUserProvider>
  )
}
export default App;